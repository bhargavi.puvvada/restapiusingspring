package com.tw.RestApiUsingSpring.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloWorldControllerTest {
    @Test
    public void testIfGreetingReturnsHelloWorld() {
        String expectedValue = "Hello World";
        HelloWorldController greet = new HelloWorldController();

        String actualValue = greet.greetings();

        assertEquals(expectedValue, actualValue);
    }
}
