package com.tw.RestApiUsingSpring.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.FileReader;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ChannelsAndProgramsControllerTest {

    static MockMvc mockMvc;

    @BeforeAll
    static void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new ChannelsAndProgramsController()).build();
    }

    @Test
    void testIfChannelsAndProgramsDataIsFetchedProperly() throws Exception {
        FileReader channelsAndProgramsData = new FileReader(Paths.get("src/main/java/com/tw/RestApiUsingSpring/repository/channels-and-programs.json").toUri().getPath());
        JSONParser jsonParser = new JSONParser();
        JSONObject expectedData = (JSONObject) jsonParser.parse(channelsAndProgramsData);

        String response = mockMvc.perform(get("/channels-and-programs"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        JSONObject actualData = (JSONObject) new JSONParser().parse(response);

        assertEquals(expectedData, actualData);

    }

    @Test
    void testIfChannelsDataIsFetchedProperly() throws Exception {
        FileReader channelsData = new FileReader(Paths.get("src/main/java/com/tw/RestApiUsingSpring/repository/channels.json").toUri().getPath());
        JSONParser jsonParser = new JSONParser();
        JSONObject expectedData = (JSONObject) jsonParser.parse(channelsData);

        String response = mockMvc.perform(get("/channels"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        JSONObject actualData = (JSONObject) new JSONParser().parse(response);

        assertEquals(expectedData, actualData);

    }

    @Test
    void testIfProgramsDataIsFetchedProperly() throws Exception {
        FileReader programsData = new FileReader(Paths.get("src/main/java/com/tw/RestApiUsingSpring/repository/programs.json").toUri().getPath());
        JSONParser jsonParser = new JSONParser();
        JSONObject expectedData = (JSONObject) jsonParser.parse(programsData);

        String response = mockMvc.perform(get("/programs"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        JSONObject actualData = (JSONObject) new JSONParser().parse(response);

        assertEquals(expectedData, actualData);

    }
}
