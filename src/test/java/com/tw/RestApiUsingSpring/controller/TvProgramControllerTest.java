package com.tw.RestApiUsingSpring.controller;

import com.tw.RestApiUsingSpring.TvProgram;
import com.tw.RestApiUsingSpring.repository.TvProgramRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class TvProgramControllerTest {

    @Autowired
    TvProgramController tvProgramController;

    @Autowired
    TvProgramRepository tvProgramRepository;

    @Test
    void testIfTvProgramDataIsFetchedProperly() {
        List<TvProgram> expected = tvProgramRepository.findAll();

        List<TvProgram> actual = tvProgramController.findAll();

        assertEquals(expected, actual);
    }
}
