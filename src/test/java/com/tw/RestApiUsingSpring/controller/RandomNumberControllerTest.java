package com.tw.RestApiUsingSpring.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RandomNumberControllerTest {

    @Test
    void testIsTrueIfNumberGeneratedIsInBetween1And10() {
        RandomNumberController randomNumberController = new RandomNumberController();

        int actual = randomNumberController.generateRandomNumber();

        Assertions.assertTrue((actual >= 1) && (actual <= 10));
    }
}
