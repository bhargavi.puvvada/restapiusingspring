package com.tw.RestApiUsingSpring.repository;

import com.tw.RestApiUsingSpring.TvProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TvProgramRepository extends JpaRepository<TvProgram,Long> {
}
