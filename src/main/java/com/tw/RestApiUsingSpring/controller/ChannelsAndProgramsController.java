package com.tw.RestApiUsingSpring.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

@RestController
public class ChannelsAndProgramsController {

    @GetMapping(value = {"/channels-and-programs", "/channels", "/programs"})
    public JSONObject getChannelsAndPrograms() throws IOException, ParseException {
        String[] fileUriString = ServletUriComponentsBuilder.fromCurrentRequest().toUriString().split("[/]");
        String fileName = fileUriString[fileUriString.length - 1];
        String filePath = "src/main/java/com/tw/RestApiUsingSpring/repository/".concat(fileName).concat(".json");
        return fetchJsonData(filePath);
    }

    private JSONObject fetchJsonData(String filePath) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = new FileReader(Paths.get(filePath).toUri().getPath());
        JSONObject data = (JSONObject) jsonParser.parse(fileReader);
        return data;
    }
}
