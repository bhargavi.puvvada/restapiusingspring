package com.tw.RestApiUsingSpring.controller;

import com.tw.RestApiUsingSpring.TvProgram;
import com.tw.RestApiUsingSpring.repository.TvProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TvProgramController {

    @Autowired
    TvProgramRepository tvProgramRepository;

    @GetMapping("/tv-programs")
    public List<TvProgram> findAll() {
        return tvProgramRepository.findAll();
    }
}
