package com.tw.RestApiUsingSpring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RandomNumberController {

    @GetMapping("/random-number")
    public int generateRandomNumber() {
        return (int) ((Math.random() * 9) + 1);
    }
}
