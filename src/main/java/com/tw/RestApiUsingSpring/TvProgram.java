package com.tw.RestApiUsingSpring;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "tv_programs")
public class TvProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "channel_name")
    private String channel_name;

    @Column(name = "timings")
    private LocalTime timings;


    public String getChannel_name() {
        return channel_name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalTime getTimings() {
        return timings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TvProgram tvProgram = (TvProgram) o;
        return id == tvProgram.id && name.equals(tvProgram.name) && channel_name.equals(tvProgram.channel_name) && timings.equals(tvProgram.timings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, channel_name, timings);
    }
}
